package
{
	import engine.Locator;
	
	import flash.display.MovieClip;
	
	public class Ball
	{
		public var model:MovieClip;
		
		public function Ball()
		{
			model = Locator.assetsManager.getMovieClip("Ball");
		}
	}
}