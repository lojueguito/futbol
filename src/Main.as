package
{
	import engine.Locator;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.ui.Keyboard;
	
	import flashx.textLayout.elements.BreakElement;
	import flashx.textLayout.events.ModelChange;
	
	import org.osmf.logging.LoggerFactory;
	
	[SWF(width="800", height="600", frameRate="30")]
	public class Main extends Locator
	{
		public var player1:Car;
		public var player2:Car;
		public var estadio:MovieClip;
		public var ball:Balon;
		public var menu:MovieClip;
		public var credits:MovieClip;
		public var how:MovieClip;
		public var textscore1:MovieClip;
		public var textscore2:MovieClip;
		public var golesRed:int;
		public var golesBlue:int;
		public var win:MovieClip;
		public var sound2:Sound;
		public var sound:Sound;
		
		public static var allBox:Vector.<Box> = new Vector.<Box>();
		public static var allRespawnBox:Vector.<RespawnBox> = new Vector.<RespawnBox>();
		public static var allPlatforms:Vector.<MovieClip> = new Vector.<MovieClip>();
		public static var allWalls:Vector.<MovieClip> = new Vector.<MovieClip>();
		public static var allSpawnsB:Vector.<MovieClip> = new Vector.<MovieClip>();
		public static var allSpawnsP:Vector.<MovieClip> = new Vector.<MovieClip>();
		public static var allSpawnsBall:Vector.<MovieClip> = new Vector.<MovieClip>();
		public static var allGoals:Vector.<MovieClip> = new Vector.<MovieClip>();
		public static var allCollisionable:Vector.<MovieClip> = new Vector.<MovieClip>();
		public var allRadians:Vector.<Number> = new Vector.<Number>();
		public static var allProyetils:Vector.<Proyectil> = new Vector.<Proyectil>();
		
		public function Main()
		{
			Locator.assetsManager.loadLinks("links01.txt");
			Locator.assetsManager.addEventListener(Event.COMPLETE, startMainMenu);	
			for(var i:int=0; i<360; i++)
			{
				allRadians.push( i * Math.PI / 180 );
			}
			
			stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			stage.scaleMode = StageScaleMode.EXACT_FIT;
			
		}
		
		protected function startMainMenu(event:Event):void
		{
			mainMenu();
		}
		
		public function mainMenu():void
		{
			menu = Locator.assetsManager.getMovieClip("MCmain");
			credits = Locator.assetsManager.getMovieClip("MCcredits");
			how=Locator.assetsManager.getMovieClip("MChow");
			stage.addChild(menu);
			menu.x=stage.stageWidth/2;
			menu.y=stage.stageHeight/2;
			menu.scaleX = menu.scaleY = 1.5;
			menu.hitplay.alpha = 0;
			menu.hithow.alpha = 0;
			menu.hitcredits.alpha = 0;
			menu.hitplay.addEventListener(MouseEvent.CLICK,clickplay);
			menu.hitcredits.addEventListener(MouseEvent.CLICK, clickCredits);
			menu.hithow.addEventListener(MouseEvent.CLICK, clickHow);
		}
		
		protected function evStartGame():void
		{
			sound = Locator.assetsManager.getSound("sndFondo");
			sound.play();
			sound2 = Locator.assetsManager.getSound("sndGoal");
			player1 = new Car(Keyboard.W,Keyboard.A,Keyboard.D,Keyboard.S,Keyboard.SPACE);
			player2 = new Car(Keyboard.UP,Keyboard.LEFT,Keyboard.RIGHT,Keyboard.DOWN,Keyboard.CONTROL);
			estadio=Locator.assetsManager.getMovieClip("Campo");
			ball = new Balon();
			stage.addChild(estadio);
			estadio.x= stage.stageWidth/2;
			estadio.y= stage.stageHeight/2;
			estadio.scaleX =1.3;
			estadio.scaleY =1.4;
			stage.addEventListener(Event.ENTER_FRAME, evUpdate);
			collectAllPlatformsFromLevel();
			spawnplayers();
			spawnboxs();
			spawnBall();
			textscore1 = Locator.assetsManager.getMovieClip("score");
			textscore2 = Locator.assetsManager.getMovieClip("score");
			stage.addChild(textscore1);
			textscore1.gotoAndStop(0);
			textscore1.x = player1.model.x;
			textscore1.y= player1.model.y;
			textscore1.scaleX = textscore1.scaleY =2;
			stage.addChild(textscore2);
			textscore2.gotoAndStop(0);
			textscore2.x = player2.model.x;
			textscore2.y= player2.model.y;
			textscore2.scaleX = textscore2.scaleY =2;


		}	
		protected function clickplay(event:MouseEvent):void
		{
			menu.hitplay.removeEventListener(MouseEvent.CLICK,clickplay);
			menu.hitcredits.removeEventListener(MouseEvent.CLICK, clickCredits);
			menu.hithow.removeEventListener(MouseEvent.CLICK, clickHow);
			stage.removeChild(menu);
			evStartGame();
		}

		protected function clickCredits(event:MouseEvent):void
		{
			stage.removeChild(menu);
			stage.addChild(credits);
			credits.x=stage.stageWidth/2;
			credits.y=stage.stageHeight/2;
			credits.scaleX = credits.scaleY = 1.6;
			credits.hitback.addEventListener(MouseEvent.CLICK,clickReturnC);
		}
		
		protected function clickReturnC(event:MouseEvent):void
		{
			credits.hitback.removeEventListener(MouseEvent.CLICK,clickReturnC);
			stage.removeChild(credits);
			stage.addChild(menu);
			
		}
		
		protected function clickHow(event:MouseEvent):void
		{
			stage.removeChild(menu);
			stage.addChild(how);
			how.x=stage.stageWidth/2;
			how.y=stage.stageHeight/2;
			how.scaleX = how.scaleY = 1.6;
			how.gotoAndStop(1);
			how.next.alpha =0;
			how.back.alpha =0;
			how.Main.addEventListener(MouseEvent.CLICK,clickHowM);
			how.next.addEventListener(MouseEvent.CLICK,clickHowN);
			how.back.addEventListener(MouseEvent.CLICK,clickHowB);
		}
		
		public function clickHowM(event:MouseEvent):void
		{
			stage.removeChild(how);
			how.Main.removeEventListener(MouseEvent.CLICK,clickHowM);
			how.next.removeEventListener(MouseEvent.CLICK,clickHowN);
			how.back.removeEventListener(MouseEvent.CLICK,clickHowB);
			stage.addChild(menu);
		}
		
		public function clickHowN(event:MouseEvent):void
		{
			/*trace("hi");*/
			if(how.currentFrame != 3)
				how.gotoAndStop(how.currentFrame + 1);
		}
		
		public function clickHowB(event:MouseEvent):void
		{
			/*trace("bye");*/
			if(how.currentFrame != 1)
				how.gotoAndStop(how.currentFrame - 1);
		}
		
		public function collectAllPlatformsFromLevel():void
		{
			for(var i:int=0; i<estadio.numChildren; i++)
			{
				if(estadio.getChildAt(i).name == "WallH")
				{
					allPlatforms.push(estadio.getChildAt(i));
					allCollisionable.push(estadio.getChildAt(i));
					estadio.getChildAt(i).visible = false;
				}
				if(estadio.getChildAt(i).name == "WallV")
				{
					allWalls.push(estadio.getChildAt(i));
					allCollisionable.push(estadio.getChildAt(i));
					estadio.getChildAt(i).visible = false;
				}
				if(estadio.getChildAt(i).name == "SpawnB")
				{
					allSpawnsB.push(estadio.getChildAt(i));
					estadio.getChildAt(i).visible = false;
				}
				if(estadio.getChildAt(i).name == "SpawnP1")
				{
					allSpawnsP.push(estadio.getChildAt(i));
					estadio.getChildAt(i).visible = false;
				}
				if(estadio.getChildAt(i).name == "SpawnP2")
				{
					allSpawnsP.push(estadio.getChildAt(i));
					estadio.getChildAt(i).visible = false;
				}
				if(estadio.getChildAt(i).name == "SpawnBall")
				{
					allSpawnsBall.push(estadio.getChildAt(i));
					estadio.getChildAt(i).visible = false;
					/*trace("entro");*/
				}
				if(estadio.getChildAt(i).name == "Goal")
				{
					allGoals.push(estadio.getChildAt(i));
					allCollisionable.push(estadio.getChildAt(i));
					estadio.getChildAt(i).visible = true;
					/*trace("entro");*/
				}
			}
		}

		
		protected function evUpdate(event:Event):void
		{
			player1.update();
			player2.update();
			ball.update();
			textscore1.alpha -=0.01;
			textscore2.alpha -=0.01;
			ballCollision();
			
			for(var i:int = 0 ; i < allBox.length; i++)
			{
				allBox[i].update();
			}
			
			for(var i:int = allRespawnBox.length-1; i >= 0 ; i--)
			{
				allRespawnBox[i].update();
				if(allRespawnBox[i].currentTimeToSpawn <= 0){
					allRespawnBox.splice(i, 1);
				}
			}
			
			if(player1.Tienecaja == true  && player1.isShoothing == true)
			{
				var caja:Proyectil = new Proyectil(player1.model.x,player1.model.y,player1.model.rotation, allProyetils.length, allCollisionable.length);
				allProyetils.push(caja);
				allCollisionable.push(caja.model);
				player1.isShoothing = false;
				player1.Tienecaja=false;
				player1.model.gotoAndStop(1);
			}
			
			if(player2.Tienecaja == true  && player2.isShoothing == true)
			{
				var caja:Proyectil = new Proyectil(player2.model.x,player2.model.y,player2.model.rotation, allProyetils.length, allCollisionable.length);
				allProyetils.push(caja);
				player2.isShoothing = false;
				player2.Tienecaja=false;
				player2.model.gotoAndStop(1);
			}
			
			for(var i:int = 0 ; i < allBox.length; i++)
			{
				if(player1.model.hitTestObject(allBox[i].model) && player1.Tienecaja== false)
				{
					var auxX:Number = allBox[i].model.x;
					var auxY:Number = allBox[i].model.y;
					player1.collicion();
					allBox[i].collicion();
					var resCaja:RespawnBox = new RespawnBox(auxX, auxY, allRespawnBox.length);
					allRespawnBox.push(resCaja);
					allBox.splice(i , 1);
				}
			}
			
			for(var i:int = 0 ; i < allBox.length; i++)
			{
				if(player2.model.hitTestObject(allBox[i].model) && player2.Tienecaja== false)
				{
					var auxX:Number = allBox[i].model.x;
					var auxY:Number = allBox[i].model.y;
					player2.collicion();
					allBox[i].collicion();
					var resCaja:RespawnBox = new RespawnBox(auxX, auxY, allRespawnBox.length);
					allRespawnBox.push(resCaja);
					allBox.splice(i , 1);
				}
			}
			
			for(var j:int = allProyetils.length-1 ; j >= 0; j--)
			{
				allProyetils[j].update();
			}
			
			if(golesRed == 2)
			{
				winner(2);
			}else if (golesBlue == 2){
				winner(1);
			}
		}
		
		public function ballCollision():void
		{
			for(var k:int=0; k<allProyetils.length; k++){
				if(allProyetils[k].model){
					if(allProyetils[k].model.hit.hitTestObject(ball.model)){
						ball.move(allProyetils[k].model.rotation);
						allProyetils[k].collicion();
						allCollisionable.splice(allProyetils[k].colN, 1);
						allProyetils.splice(allProyetils[k].proyN, 1)
					}
				}
			}
			
			for(var i:int=0; i<allWalls.length; i++)
			{
				if(ball.model.hitTestObject(allWalls[i])){
					ball.direction.x *= -1;
				}
			}
			
			for(var i:int=0; i<allPlatforms.length; i++)
			{
				if(ball.model.hitTestObject(allPlatforms[i])){
					ball.direction.y *= -1;
				}
			}
			
			for(var i:int = 0; i<allGoals.length; i++)
			{
				if(ball.model.hitTestObject(allGoals[i])){
					if(ball.model.x < Locator.mainStage.stageWidth/2)
					{
						golesBlue ++;
						textscore2.gotoAndStop(golesBlue+1);
						textscore2.alpha = 1;
						sound2.play();
							//Punto para el player 2
					} else {
						golesRed ++;
						textscore1.gotoAndStop(golesRed+1);
						textscore1.alpha = 1;
						sound2.play();
						//Punto para el player 1
					}
					restarPositions();
				}
			}
		}
		
		public function restarPositions():void
		{
			for(var i:int = allSpawnsP.length-1; i >= 0; i--)
			{
				if(allSpawnsP[i].name == "SpawnP1")
				{
					var kLocal:Point = new Point(allSpawnsP[i].x, allSpawnsP[i].y);
					var kGlobal:Point = allSpawnsP[i].parent.localToGlobal(kLocal);
					player1.model.x = kGlobal.x;
					player1.model.y = kGlobal.y;
					player1.model.rotation = 0;
					player1.speed = 0;
					player1.Tienecaja=false;
					player1.model.gotoAndStop(1);
				}
				else if(allSpawnsP[i].name == "SpawnP2")
				{
					var kLocal:Point = new Point(allSpawnsP[i].x, allSpawnsP[i].y);
					var kGlobal:Point = allSpawnsP[i].parent.localToGlobal(kLocal);
					player2.model.x = kGlobal.x;
					player2.model.y = kGlobal.y;
					player2.model.rotation = 180;
					player2.speed = 0;
					player2.Tienecaja=false;
					player2.model.gotoAndStop(1);
				}
			}
			
			for(var i:int = allSpawnsBall.length-1; i >= 0; i--)
			{
				if(allSpawnsBall[i].name == "SpawnBall")
				{
					var pLocal:Point = new Point(allSpawnsBall[i].x, allSpawnsBall[i].y);
					var pGlobal:Point = allSpawnsBall[i].parent.localToGlobal(pLocal);
					ball.model.x =pGlobal.x;
					ball.model.y =pGlobal.y;
					ball.speed = 0;
				} 
			}
		}
		
		public function spawnboxs():void
		{
			for(var i:int = allSpawnsB.length-1; i >= 0; i--)
			{
				if(allSpawnsB[i].name == "SpawnB")
				{
					var pLocal:Point = new Point(allSpawnsB[i].x, allSpawnsB[i].y);
					var pGlobal:Point = allSpawnsB[i].parent.localToGlobal(pLocal);
					var box:Box = new Box();
					box.spawn(pGlobal.x, pGlobal.y);
					allBox.push(box);
					
				} 
			}
		}
		
		public function spawnplayers():void
		{
			for(var i:int = allSpawnsP.length-1; i >= 0; i--)
			{
				if(allSpawnsP[i].name == "SpawnP1")
				{
					var kLocal:Point = new Point(allSpawnsP[i].x, allSpawnsP[i].y);
					var kGlobal:Point = allSpawnsP[i].parent.localToGlobal(kLocal);
					player1.spawn("Player1", kGlobal.x , kGlobal.y);
				}else if(allSpawnsP[i].name == "SpawnP2")
				{
					var pLocal:Point = new Point(allSpawnsP[i].x, allSpawnsP[i].y);
					var pGlobal:Point = allSpawnsP[i].parent.localToGlobal(pLocal);
					player2.spawn("Player2", pGlobal.x , pGlobal.y);
					player2.model.rotation =180;
				}
			}
		}
		
		public function spawnBall():void
		{
			for(var i:int = allSpawnsBall.length-1; i >= 0; i--)
			{
				if(allSpawnsBall[i].name == "SpawnBall")
				{
					var pLocal:Point = new Point(allSpawnsBall[i].x, allSpawnsBall[i].y);
					var pGlobal:Point = allSpawnsBall[i].parent.localToGlobal(pLocal);
					stage.addChild(ball.model);
					allCollisionable.push(ball.model);
					ball.model.x =pGlobal.x;
					ball.model.y =pGlobal.y;
				} 
			}
		}
		
		public function winner(j:int):void
		{
			win = Locator.assetsManager.getMovieClip("Win");
			stage.addChild(win);
			win.x = stage.stageWidth/2;
			win.y = stage.stageHeight/2;
			win.scaleX = win.scaleY = 1;
			win.gotoAndStop(j);
			stage.addEventListener(MouseEvent.CLICK,returned);
		}
		
		public function returned(event:MouseEvent):void
		{
			stage.removeEventListener(MouseEvent.CLICK, returned);
			cleanAll();
			mainMenu();
			/*stage.addChild(menu);*/
		}
		
		public function cleanAll(){
			golesRed = 0;
			golesBlue = 0;
			
			allBox = new Vector.<Box>();
			allRespawnBox = new Vector.<RespawnBox>();
			allPlatforms = new Vector.<MovieClip>();
			allWalls = new Vector.<MovieClip>();
			allSpawnsB = new Vector.<MovieClip>();
			allSpawnsP = new Vector.<MovieClip>();
			allSpawnsBall = new Vector.<MovieClip>();
			allGoals = new Vector.<MovieClip>();
			allCollisionable = new Vector.<MovieClip>();
			allProyetils = new Vector.<Proyectil>();
			
			for (var i:int = Locator.mainStage.numChildren-1; i > 0; i--) {
				trace(Locator.mainStage.getChildAt(i));
				Locator.mainStage.removeChildAt(i);
			}
		}
	}
}