package
{
	import engine.Locator;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;

	public class Proyectil extends Box
	{
		public var speed:Number;
		public var isShooting:Boolean;
		public var radians:Number;
		public var degrees:Number;
		public var proyN:int;
		public var colN:int;
		public var direction:Point = new Point();
		/*public var ballModel:MovieClip = */
		
		public function Proyectil(posx:Number,posy:Number,rotation:Number, proyN:int, colN:int)
		{
			super.spawn(posx, posy);
			this.proyN = proyN;
			this.colN = colN;
			speed = 7.5;
			
			radians = rotation * Math.PI / 180;
			degrees = rotation;
			model.x=posx;
			model.y=posy;
			
			model.rotation = degrees;
			direction.x = Math.cos(radians);
			direction.y = Math.sin(radians);

		}
		
		override public function update():void
		{
			if(model){
				model.x += direction.x * speed;
				model.y += direction.y * speed;
				for(var i:int = Main.allCollisionable.length-1; i>=0; i--){
					if(i != colN && model != null && model.hitTestObject(Main.allCollisionable[i]))
					{
						if(Main.allCollisionable[i].name == "WallH" || Main.allCollisionable[i].name == "WallV" || Main.allCollisionable[i].name == "Goal"){
							super.collicion();
							Main.allCollisionable.splice(colN, 1);
							Main.allProyetils.splice(proyN, 1);
						}
					}
				}
			}
		}
/*		public function Spawn (J:MovieClip):void
		{
			trace("Shooteo")
			Locator.mainStage.addChild(model);
			model.x =J.x;
			model.y=J.y;
			isShooting=true;
		}*/
		/*public function Bullet(posX:Number, posY:Number)
		{
			model = Locator.assetsManager.getMovieClip("MCBullet");
			Locator.mainStage.addChild(model);
			model.x = posX;
			model.y = posY;
		}
		public function update (posx:int,posy:int):void
		{
			model.x += direction.x * speed;
			model.y += direction.y * speed;
		}
		public function CanonBullet(posX:Number, posY:Number, rotation:Number)
		{
			super(posX, posY);
			
			radians = rotation * Math.PI / 180;
			degrees = rotation;
			
			direction.x = Math.cos(radians);
			direction.y = Math.sin(radians);
			
			model.rotation = degrees;
		}
		
		override public function update():void
		{
			
		}*/
	}
}