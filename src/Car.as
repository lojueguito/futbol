package
{
	import engine.Locator;
	
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;

	public class Car
	{
		public var speed:Number;
		public var MAXspeed:int = 10;
		public var minSpeed:int = -5;
		public var tiro:int;
		public var model:MovieClip;
		public var Tienecaja:Boolean;
		
		public var isAcceleration:Boolean=false;
		public var isPressedLeft:Boolean;
		public var isPressedRight:Boolean;
		public var isPressedForward:Boolean;
		public var isPressedBack:Boolean;
		public var isShoothing:Boolean;
		
		public var keyForward:uint; 
		public var keyLeft:uint;
		public var keyRight:uint;
		public var keyBack:uint;
		public var keyShoot:uint;
		
		public function Car(keyForward:uint, keyLeft:uint, keyRight:uint, keyBack:uint, keyShoot:uint)
		{
			
			speed = 0;
			this.keyForward = keyForward;
			this.keyLeft = keyLeft;
			this.keyRight = keyRight;
			this.keyBack = keyBack;
			this.keyShoot = keyShoot;
		}
		
		public function spawn(j:String,posx:int ,posy:int):void
		{
			model= Locator.assetsManager.getMovieClip(j);
			model.x =posx;
			model.y =posy;
			Locator.mainStage.addChild(model);
			model.scaleX = model.scaleY = 0.75;
			model.hit.visible = false;
			model.gotoAndStop(1);
			
			Locator.mainStage.addEventListener(KeyboardEvent.KEY_UP, evKeyUp);
			Locator.mainStage.addEventListener(KeyboardEvent.KEY_DOWN, evKeyDown);
		}
		
		public function move():void
		{
				var direction:Point = new Point();
				direction.x = Math.cos(model.rotation * Math.PI / 180);
				direction.y = Math.sin(model.rotation * Math.PI / 180);
				model.x += direction.x * speed;
				model.y += direction.y * speed;
				if(isAcceleration && speed <= MAXspeed){
					if(speed > MAXspeed)
						speed=MAXspeed;
					else
						speed += 0.25;
				}
				else if (!isAcceleration && speed > 0 && !isPressedBack){
					if(speed < 0)
						speed = 0;	
					else
						speed -= 0.25;
				}
				if(isPressedBack && speed>=minSpeed)
				{
					
					if(speed < minSpeed)
						speed = minSpeed;	
					else
						speed -= 0.5;
					
				} else if(!isPressedBack && speed < 0){
					if(speed < 0)
						speed += 0.25;	
					else
						speed = 0;
				}
		}	
		
		public function update():void
		{
			if(isPressedLeft){
				model.rotation -= 5;
			} else if(isPressedRight) {
				model.rotation += 5;
			}
			
			move();
			
			
			for(var j:int = Main.allPlatforms.length-1; j>=0; j--){
				if(model.hit.hitTestObject(Main.allPlatforms[j])){
					if(model.y< 100)
						model.y +=25;
					else if (model.y > 100)
						model.y -=25;
				}
			}
			
			for(var k:int = Main.allWalls.length-1; k>=0;k--){
				if(model.hit.hitTestObject(Main.allWalls[k])){
					if(model.x< 100)
						model.x +=25;
					else if (model.x > 100)
						model.x -=25;
				}
			}
			for(var w:int = Main.allGoals.length-1; w>=0;w--){
				if(model.hit.hitTestObject(Main.allGoals[w])){
					if(model.x< 100)
						model.x +=25;
					else if (model.x > 100)
						model.x -=25;
				}
			}
		}
		
		public function collicion():void 
		{
			Tienecaja = true;
			model.gotoAndStop(2);
		}
		
		protected function evKeyDown(event:KeyboardEvent):void
		{
			switch(event.keyCode)
			{
				case keyShoot:
					isShoothing=true;
					break;
				
				case keyLeft:
					isPressedLeft = true;
					break;
				
				case keyRight:
					isPressedRight = true;
					break;
				
				case keyForward:
					isAcceleration =true;
					isPressedForward = true;
					break;
				
				case keyBack:
					isPressedBack = true;
					break;
			}
		}
		
		protected function evKeyUp(event:KeyboardEvent):void
		{
			switch(event.keyCode)
			{
				case keyShoot:
					isShoothing=false;
					break;
				
				case keyLeft:
					isPressedLeft = false;
					break;
				
				case keyRight:
					isPressedRight = false;
					break;
				
				case keyForward:
					isAcceleration =false;
					isPressedForward = false;
					break;
				
				case keyBack:
					isPressedBack = false;
					break;
			}
			
		}
		
	}
}