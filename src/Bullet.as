package
{
	import flash.display.MovieClip;
	import flash.geom.Point;
	
	import engine.Locator;

	public class Bullet
	{
		public var model:MovieClip;
		public var speed:Number = 7;
		public var radians:Number;
		public var degrees:Number;
		public var direction:Point = new Point();
		public var distance:Point = new Point();
		
		public function Bullet(posX:Number, posY:Number)
		{
			trace("LEAAAA SE PUDO");
			model = Locator.assetsManager.getMovieClip("MCBullet");
			Locator.mainStage.addChild(model);
			model.x = posX;
			model.y = posY;
		}
		
		public function update():void
		{
			
		}
	}
}