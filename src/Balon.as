package
{
	import engine.Locator;
	
	import flash.display.MovieClip;
	import flash.geom.Point;

	public class Balon
	{
		public var model:MovieClip;
		public var speed:Number = 0;
		public var radians:Number;
		public var degrees:Number;
		public var nMovement:Number;
		public var iMovement:int;
		public var direction:Point = new Point();
		
		public function Balon()
		{
			model = Locator.assetsManager.getMovieClip("Ball");
			model.gotoAndStop(1);
		}
		
		public function update():void{
			model.x += direction.x * speed;
			model.y += direction.y * speed;
			if(speed > 0){
				speed -= 0.1;
			} else {
				speed = 0;
			}
			for(var i:int = Main.allCollisionable.length-1; i>=0; i--){
				if(model != null && model.hitTestObject(Main.allCollisionable[i]))
				{
					/*trace("Hola");*/
				}
			}
		}
		
		public function move(rotation:Number):void{
			radians = rotation * Math.PI / 180;
			degrees = rotation;
			
			model.rotation = degrees;
			direction.x = Math.cos(radians);
			direction.y = Math.sin(radians);
			speed = 5;
		}
	}
}