package
{
	import flash.geom.Point;

	public class Granade extends Bullet
	{
		public var target:Point = new Point();
		
		public function Granade(posX:Number, posY:Number, targetX:Number, targetY:Number)
		{
			super(posX, posY);
			
			target.x = targetX;
			target.y = targetY;
		}
		
		override public function update():void
		{
			//model.rotation += 5;
			
			distance.y = target.y - model.y;
			distance.x = target.x - model.x;
			radians = Math.atan2(distance.y, distance.x);
			degrees = radians * 180 / Math.PI;
			
			direction.x = Math.cos(radians);
			direction.y = Math.sin(radians);
			
			model.rotation = degrees;
			
			if(Math.abs(distance.x) > speed || Math.abs(distance.y) > speed)
			{
				model.x += direction.x * speed;
				model.y += direction.y * speed;
			}
		}
	}
}