package
{
	import flash.display.MovieClip;

	public class Shield extends Bullet
	{
		public var objectProtected:MovieClip;
		public var radius:Number;
		
		public function Shield(objectProtected:MovieClip, radius:Number)
		{
			super(objectProtected.x, objectProtected.y);
			
			this.objectProtected = objectProtected;
			this.radius = radius;
			
			degrees = 0;
		}
		
		override public function update():void
		{
			degrees += speed;
			radians = degrees * Math.PI / 180;
			
			model.x = objectProtected.x + Math.cos(radians) * radius;
			model.y = objectProtected.y + Math.sin(radians) * radius;
		}
	}
}