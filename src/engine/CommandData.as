package engine
{
	public class CommandData
	{
		public var name:String;
		public var command:Function;
		public var description:String;
		public var numArgs:int;
	}
}