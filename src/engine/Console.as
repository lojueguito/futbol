package engine
{
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;

	public class Console
	{
		public var model:MCConsole;
		public var keyForOpenConsole:int;
		public var isOpened:Boolean;
		public var allCommands:Dictionary = new Dictionary();
		
		public function Console()
		{
			trace("Inicializando Consola...");
			model = new MCConsole();
			
			Locator.mainStage.addEventListener(KeyboardEvent.KEY_UP, evKeyUp);
			keyForOpenConsole = Keyboard.F8;
			
			registerCommand("cls", clear, "Esto limpia la pantalla.", 0);
			registerCommand("exit", exit, "Esto sale del juego.", 0);
			registerCommand("help", help, "Esto muestra lo que estás viendo ahora al tipear esto.",0);
		}
		
		protected function evKeyUp(event:KeyboardEvent):void
		{
			if(event.keyCode == keyForOpenConsole)
			{
				//       IF       ELSE
				!isOpened ? open() : close();
				isOpened = !isOpened;
				
				
				//var isWin:Boolean = score > 1000 ? true : false;
				
				/*if(!isOpened)
				{
					open();
					isOpened = true;
				}else
				{
					close();
					isOpened = false;
				}*/
			}else if(isOpened && event.keyCode == Keyboard.ENTER)
			{
				execCommand();
				model.tb_input.text = "";
			}
		}
		
		public function execCommand():void
		{
			var textParsed:Array = model.tb_input.text.split(" ");
			var commandName:String = textParsed[0];
			var temp:CommandData = allCommands[commandName];
			
			textParsed.splice(0, 1); //Elimino el nombre del comando.
			
			if(temp != null)
			{
				try
				{
					temp.command();
				}catch(e1:ArgumentError)
				{
					try
					{
						temp.command.apply(this, textParsed);
					}catch(e3:Error)
					{
						writeLn("Cantidad de parámetros incorrecta.");
					}
				}catch(e2:Error)
				{
					writeLn("Tu función crasheo amego.");
				}
			}else
			{
				writeLn("Recatate loco. El comando no siste.");
			}
		}
		
		public function clear():void
		{
			model.tb_log.text = "";
		}
		
		public function exit():void
		{
			
		}
		
		public function help():void
		{
			writeLn("Lista de ayuda: ");
			writeLn("");
			for(var varName:String in allCommands)
			{
				//trace(varName, allCommands[varName]);
				writeLn(varName + " : " + allCommands[varName].description);
			}
		}
		
		public function registerCommand(name:String, command:Function, description:String, numArgs:int):void
		{
			var cData:CommandData = new CommandData();
			cData.command = command;
			cData.name = name;
			cData.description = description;
			cData.numArgs = numArgs;
			
			allCommands[name] = cData;
		}
		
		public function unregisterCommand(name:String):void
		{
			delete allCommands[name];
		}
		
		public function write(text:String):void
		{
			model.tb_log.text += text;
		}
		
		public function writeLn(text:String):void
		{
			write(text + "\n");
		}
		
		public function open():void
		{
			Locator.mainStage.addChild(model);
			Locator.mainStage.focus = model.tb_input;
		}
		
		public function close():void
		{
			Locator.mainStage.removeChild(model);
			Locator.mainStage.focus = Locator.mainStage;
		}
	}
}