package
{
	import flash.geom.Point;
	
	public class CanonBullet extends Bullet
	{
		public var target:Point = new Point();
		
		public function CanonBullet(posX:Number, posY:Number, rotation:Number)
		{
			super(posX, posY);
			
			radians = rotation * Math.PI / 180;
			degrees = rotation;
			
			direction.x = Math.cos(radians);
			direction.y = Math.sin(radians);
			
			model.rotation = degrees;
		}
		
		override public function update():void
		{
			model.x += direction.x * speed;
			model.y += direction.y * speed;
		}
	}
}