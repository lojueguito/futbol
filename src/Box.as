package
{
	import engine.Locator;
	
	import flash.display.MovieClip;

	public class Box
	{	
		public var model:MovieClip;
		
		public function Box()
		{
		}
		public function spawn(posx:Number, posy:Number):void
		{
			model =Locator.assetsManager.getMovieClip("MCbox")
			Locator.mainStage.addChild(model);
			model.x = posx;
			model.y =posy;
			model.scaleX = model.scaleY = 0.5;
			model.hit.visible = false;
		}
		public function collicion():void
		{
			this.model.parent.removeChild(this.model);
			model= null;
		}
	
		public function update():void
		{
			
		}
	}
}