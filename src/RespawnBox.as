package
{
	import engine.Locator;
	
	import flash.display.MovieClip;
	
	import org.osmf.media.LoadableElementBase;

	public class RespawnBox
	{	
		public var posX:Number;
		public var posY:Number;
		public var currentTimeToSpawn:Number;
		public var timeToSpawn:int;
		public var resN:int;
		
		public function RespawnBox(posX:Number, posY:Number, resN:int)
		{
			this.posX = posX;
			this.posY = posY;
			this.resN = resN;
			timeToSpawn = 2000;
			currentTimeToSpawn = timeToSpawn;
		}
	
		public function update():void
		{
			if(currentTimeToSpawn > 0)
				currentTimeToSpawn -= 1000/Locator.mainStage.frameRate;
			trace(currentTimeToSpawn);
			if(currentTimeToSpawn <= 0){
				var caja:Box = new Box();
				/*Main.allRespawnBox.splice(resN, 1);*/
				caja.spawn(posX, posY);
				Main.allBox.push(caja);
			}
		}
	}
}